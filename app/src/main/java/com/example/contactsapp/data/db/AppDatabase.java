package com.example.contactsapp.data.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.contactsapp.data.db.model.Contact;

@Database(entities = {Contact.class}, version = AppDatabase.VERSION)
public abstract class AppDatabase extends RoomDatabase {

    static final int VERSION = 1;

    public abstract ContactsDao getContactsDao();
}
