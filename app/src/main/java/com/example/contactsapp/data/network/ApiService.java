package com.example.contactsapp.data.network;


import com.example.contactsapp.data.network.model.ContactModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    @GET("users")
    Single<List<ContactModel>> getUsers(@Query("size") int size);
}