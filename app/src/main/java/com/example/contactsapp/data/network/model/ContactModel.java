package com.example.contactsapp.data.network.model;

import java.io.Serializable;

public class ContactModel implements Serializable {
    private Address address;

    private String gender;

    private String date_of_birth;

    private String last_name;

    private String avatar;

    private Employment employment;

    private Subscription subscription;

    private String uid;

    private String password;

    private Credit_card credit_card;

    private String social_insurance_number;

    private String phone_number;

    private Integer id;

    private String first_name;

    private String email;

    private String username;

    private Boolean isFavourite = false;

    public void setIsFavourite(Boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

    public Boolean getIsFavourite() {
        return this.isFavourite;
    }

    public Address getAddress() {
        return this.address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDate_of_birth() {
        return this.date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getLast_name() {
        return this.last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Employment getEmployment() {
        return this.employment;
    }

    public void setEmployment(Employment employment) {
        this.employment = employment;
    }

    public Subscription getSubscription() {
        return this.subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public String getUid() {
        return this.uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Credit_card getCredit_card() {
        return this.credit_card;
    }

    public void setCredit_card(Credit_card credit_card) {
        this.credit_card = credit_card;
    }

    public String getSocial_insurance_number() {
        return this.social_insurance_number;
    }

    public void setSocial_insurance_number(String social_insurance_number) {
        this.social_insurance_number = social_insurance_number;
    }

    public String getPhone_number() {
        return this.phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirst_name() {
        return this.first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static class Address implements Serializable {
        private String street_address;

        private String country;

        private String city;

        private Coordinates coordinates;

        private String state;

        private String street_name;

        private String zip_code;

        public String getStreet_address() {
            return this.street_address;
        }

        public void setStreet_address(String street_address) {
            this.street_address = street_address;
        }

        public String getCountry() {
            return this.country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCity() {
            return this.city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public Coordinates getCoordinates() {
            return this.coordinates;
        }

        public void setCoordinates(Coordinates coordinates) {
            this.coordinates = coordinates;
        }

        public String getState() {
            return this.state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getStreet_name() {
            return this.street_name;
        }

        public void setStreet_name(String street_name) {
            this.street_name = street_name;
        }

        public String getZip_code() {
            return this.zip_code;
        }

        public void setZip_code(String zip_code) {
            this.zip_code = zip_code;
        }

        public static class Coordinates implements Serializable {
            private Double lng;

            private Double lat;

            public Double getLng() {
                return this.lng;
            }

            public void setLng(Double lng) {
                this.lng = lng;
            }

            public Double getLat() {
                return this.lat;
            }

            public void setLat(Double lat) {
                this.lat = lat;
            }
        }
    }

    public static class Employment implements Serializable {
        private String key_skill;

        private String title;

        public String getKey_skill() {
            return this.key_skill;
        }

        public void setKey_skill(String key_skill) {
            this.key_skill = key_skill;
        }

        public String getTitle() {
            return this.title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class Subscription implements Serializable {
        private String term;

        private String plan;

        private String payment_method;

        private String status;

        public String getTerm() {
            return this.term;
        }

        public void setTerm(String term) {
            this.term = term;
        }

        public String getPlan() {
            return this.plan;
        }

        public void setPlan(String plan) {
            this.plan = plan;
        }

        public String getPayment_method() {
            return this.payment_method;
        }

        public void setPayment_method(String payment_method) {
            this.payment_method = payment_method;
        }

        public String getStatus() {
            return this.status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public static class Credit_card implements Serializable {
        private String cc_number;

        public String getCc_number() {
            return this.cc_number;
        }

        public void setCc_number(String cc_number) {
            this.cc_number = cc_number;
        }
    }
}
