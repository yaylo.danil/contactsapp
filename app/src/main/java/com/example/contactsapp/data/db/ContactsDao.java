package com.example.contactsapp.data.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.contactsapp.data.db.model.Contact;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

@Dao
public interface ContactsDao {
    @Query("SELECT * FROM Contact")
    Maybe<List<Contact>> getAll();

    @Query("SELECT EXISTS(SELECT * FROM Contact WHERE id = :id)")
    Single<Boolean> isContactExist(Integer id);

    @Insert
    Completable insert(Contact contact);

    @Delete
    Completable delete(Contact contact);
}
