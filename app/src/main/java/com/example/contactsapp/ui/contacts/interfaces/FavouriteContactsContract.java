package com.example.contactsapp.ui.contacts.interfaces;

import com.example.contactsapp.data.network.model.ContactModel;

import java.util.List;

public interface FavouriteContactsContract {
    interface View {
        void showFavouriteContacts(List<ContactModel> loginResponseModel);
    }

    interface Presenter {
        void attachView(FavouriteContactsContract.View view);

        void detachView();

        void addToFavourites(ContactModel contactModel);

        void removeFromFavourites(ContactModel contactModel);

        void getAllFavourites();
    }
}
