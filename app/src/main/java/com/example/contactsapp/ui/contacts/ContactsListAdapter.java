package com.example.contactsapp.ui.contacts;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.contactsapp.data.network.model.ContactModel;
import com.example.contactsapp.databinding.ContactItemLayoutBinding;
import com.example.contactsapp.ui.contacts.interfaces.ContactOnClickListener;

import java.util.ArrayList;
import java.util.List;

public class ContactsListAdapter extends RecyclerView.Adapter<ContactViewHolder> {
    public ContactsListAdapter(ContactOnClickListener listener) {
        this.listener = listener;
    }

    private final ContactOnClickListener listener;
    private List<ContactModel> contactsList = new ArrayList<>();

    public void setList(List<ContactModel> list) {
        contactsList = list;
        notifyDataSetChanged();
    }

    public void setIsFavouriteItem(Boolean isFavourite, Integer index) {
        contactsList.get(index).setIsFavourite(isFavourite);
        notifyItemChanged(index);
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ContactItemLayoutBinding binding = ContactItemLayoutBinding.inflate(LayoutInflater.from(parent.getContext()));
        binding.getRoot().setLayoutParams(parent.getLayoutParams());
        return new ContactViewHolder(binding, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        holder.bind(contactsList.get(position));
    }

    @Override
    public int getItemCount() {
        return contactsList.size();
    }
}

class ContactViewHolder extends RecyclerView.ViewHolder {
    private final ContactItemLayoutBinding binding;
    private final ContactOnClickListener listener;

    public ContactViewHolder(ContactItemLayoutBinding binding, ContactOnClickListener listener) {
        super(binding.getRoot());
        this.listener = listener;
        this.binding = binding;
    }

    void bind(ContactModel item) {
        binding.firstName.setText(item.getFirst_name());
        binding.lastName.setText(item.getLast_name());
        binding.phoneNumber.setText(item.getPhone_number());
        binding.favouriteBookmark.setChecked(item.getIsFavourite());

        binding.getRoot().setOnClickListener(view -> listener.onClick(item));
        binding.favouriteBookmark.setOnCheckedChangeListener((compoundButton, b) -> {
            listener.onCheckChanged(item, b);
        });
    }
}
