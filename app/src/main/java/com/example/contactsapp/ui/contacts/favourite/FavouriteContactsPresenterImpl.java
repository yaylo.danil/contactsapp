package com.example.contactsapp.ui.contacts.favourite;

import com.example.contactsapp.data.db.ContactsDao;
import com.example.contactsapp.data.network.model.ContactModel;
import com.example.contactsapp.ui.contacts.interfaces.FavouriteContactsContract;
import com.example.contactsapp.ui.contacts.interfaces.FavouritesContactsDataListener;

import java.util.List;

import javax.inject.Inject;

public class FavouriteContactsPresenterImpl implements FavouriteContactsContract.Presenter, FavouritesContactsDataListener {

    private final FavouriteContactsModel model;
    private FavouriteContactsContract.View view;

    @Inject
    public FavouriteContactsPresenterImpl(ContactsDao contactsDao) {
        this.model = new FavouriteContactsModel(contactsDao, this);
    }

    @Override
    public void attachView(FavouriteContactsContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void addToFavourites(ContactModel contactModel) {
        model.insertContact(contactModel);
    }

    @Override
    public void removeFromFavourites(ContactModel contactModel) {
        model.removeContact(contactModel);
    }

    @Override
    public void getAllFavourites() {
        model.getFavourites();
    }

    @Override
    public void onDataLoaded(List<ContactModel> list) {
        view.showFavouriteContacts(list);
    }

    @Override
    public void onError(Throwable throwable) {
        throwable.printStackTrace();
    }
}
