package com.example.contactsapp.ui.contacts.all;

import com.example.contactsapp.data.db.ContactsDao;
import com.example.contactsapp.data.network.ApiService;
import com.example.contactsapp.data.network.model.ContactModel;
import com.example.contactsapp.ui.contacts.interfaces.AllContactsContract;
import com.example.contactsapp.ui.contacts.interfaces.AllContactsDataListener;

import java.util.List;

import javax.inject.Inject;

public class AllContactsPresenterImpl implements AllContactsContract.Presenter, AllContactsDataListener {

    private final AllContactsModel model;
    private AllContactsContract.View view;

    @Inject
    public AllContactsPresenterImpl(ApiService api, ContactsDao contactsDao) {
        this.model = new AllContactsModel(api, contactsDao, this);
    }

    @Override
    public void attachView(AllContactsContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void loadData() {
        model.loadData();
    }

    @Override
    public void addToFavourites(ContactModel contactModel) {
        model.insertContact(contactModel);
    }

    @Override
    public void removeFromFavourites(ContactModel contactModel) {
        model.removeContact(contactModel);
    }

    @Override
    public void checkIsFavourite(Integer index, ContactModel contactModel) {
        model.checkIsFavourite(contactModel, index);
    }

    @Override
    public void onDataLoaded(List<ContactModel> list) {
        view.showLoadedData(list);
    }

    @Override
    public void isContactChecked(Boolean isChecked, Integer index) {
        view.setIsChecked(index, isChecked);
    }

    @Override
    public void onError(Throwable throwable) {
        throwable.printStackTrace();
    }
}
