package com.example.contactsapp.ui.contacts;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.contactsapp.ui.contacts.all.AllContactsPageFragment;
import com.example.contactsapp.ui.contacts.favourite.FavouriteContactsPageFragment;

public class ContactsPagerAdapter extends FragmentStateAdapter {
    private static final int PAGES_COUNT = 2;

    public ContactsPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @Override
    public Fragment createFragment(int position) {
        Fragment pageFragment;
        if (position == 0) {
            pageFragment = new AllContactsPageFragment();
        } else {
            pageFragment = new FavouriteContactsPageFragment();
        }
        return pageFragment;
    }

    @Override
    public int getItemCount() {
        return PAGES_COUNT;
    }
}
