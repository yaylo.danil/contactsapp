package com.example.contactsapp.ui.contacts.interfaces;

import com.example.contactsapp.data.network.model.ContactModel;

public interface ContactOnClickListener {
    void onClick(ContactModel data);

    void onCheckChanged(ContactModel contact, Boolean isChecked);
}
