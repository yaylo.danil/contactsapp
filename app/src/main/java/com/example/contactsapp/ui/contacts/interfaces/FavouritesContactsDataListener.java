package com.example.contactsapp.ui.contacts.interfaces;

import com.example.contactsapp.data.network.model.ContactModel;

import java.util.List;

public interface FavouritesContactsDataListener {
    void onDataLoaded(List<ContactModel> list);

    void onError(Throwable throwable);
}
