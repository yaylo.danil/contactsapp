package com.example.contactsapp.ui.contacts.favourite;

import android.annotation.SuppressLint;
import android.provider.ContactsContract;

import com.example.contactsapp.data.db.ContactsDao;
import com.example.contactsapp.data.db.model.Contact;
import com.example.contactsapp.data.network.model.ContactModel;
import com.example.contactsapp.ui.contacts.interfaces.FavouritesContactsDataListener;
import com.example.contactsapp.utils.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FavouriteContactsModel {
    private final ContactsDao contactsDao;
    private final FavouritesContactsDataListener listener;

    private final ModelMapper mapper = new ModelMapper();

    public FavouriteContactsModel(ContactsDao contactsDao, FavouritesContactsDataListener listener) {
        this.contactsDao = contactsDao;
        this.listener = listener;
    }

    @SuppressLint("CheckResult")
    public void getFavourites() {
        contactsDao.getAll()
                .subscribeOn(Schedulers.io())
                .map(contacts -> {
                    List<ContactModel> resultList = new ArrayList<>();
                    for (Contact contact : contacts) {
                        resultList.add(mapper.mapDbToNetwork(contact, true));
                    }
                    return resultList;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        listener::onDataLoaded,
                        listener::onError
                );
    }

    @SuppressLint("CheckResult")
    public void insertContact(ContactModel contact) {
        contactsDao.insert(mapper.mapNetworkToDb(contact))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(Throwable::printStackTrace)
                .subscribe();
    }

    @SuppressLint("CheckResult")
    public void removeContact(ContactModel contact) {
        contactsDao.delete(mapper.mapNetworkToDb(contact))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(Throwable::printStackTrace)
                .subscribe();
    }
}
