package com.example.contactsapp.ui.contacts.favourite;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.contactsapp.BaseApplication;
import com.example.contactsapp.R;
import com.example.contactsapp.data.network.model.ContactModel;
import com.example.contactsapp.databinding.FragmentFavouriteContactsBinding;
import com.example.contactsapp.ui.contacts.ContactsListAdapter;
import com.example.contactsapp.ui.contacts.interfaces.ContactOnClickListener;
import com.example.contactsapp.ui.contacts.interfaces.FavouriteContactsContract;
import com.example.contactsapp.ui.details.ContactDetailsFragment;
import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

public class FavouriteContactsPageFragment extends Fragment implements FavouriteContactsContract.View, ContactOnClickListener {
    private FragmentFavouriteContactsBinding binding;
    private ContactsListAdapter adapter;

    @Inject
    FavouriteContactsContract.Presenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentFavouriteContactsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupList();
    }

    private void setupList() {
        binding.contactsList.setLayoutManager(new LinearLayoutManager(requireContext()));
        adapter = new ContactsListAdapter(this);
        binding.contactsList.setAdapter(adapter);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ((BaseApplication) requireActivity().getApplication()).getAppComponent().inject(this);
        presenter.attachView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getAllFavourites();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
        presenter.detachView();
    }

    @Override
    public void showFavouriteContacts(List<ContactModel> contacts) {
        adapter.setList(contacts);
    }

    @Override
    public void onClick(ContactModel data) {
        String contactData = new Gson().toJson(data);
        Bundle bundle = new Bundle();
        bundle.putString(ContactDetailsFragment.CONTACT_DATA_TAG, contactData);

        NavHostFragment.findNavController(this).navigate(R.id.contactDetailsFragment, bundle);
    }

    @Override
    public void onCheckChanged(ContactModel contact, Boolean isChecked) {
        if (isChecked) {
            presenter.addToFavourites(contact);
        } else {
            presenter.removeFromFavourites(contact);
        }
    }
}
