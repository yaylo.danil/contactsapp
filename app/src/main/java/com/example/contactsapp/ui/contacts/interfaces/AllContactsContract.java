package com.example.contactsapp.ui.contacts.interfaces;

import com.example.contactsapp.data.network.model.ContactModel;

import java.util.List;

public interface AllContactsContract {
    interface View {
        void showLoadedData(List<ContactModel> loginResponseModel);

        void setIsChecked(Integer index, Boolean isChecked);
    }

    interface Presenter {
        void attachView(AllContactsContract.View view);

        void detachView();

        void loadData();

        void addToFavourites(ContactModel contactModel);

        void removeFromFavourites(ContactModel contactModel);

        void checkIsFavourite(Integer index, ContactModel contactModel);
    }
}
