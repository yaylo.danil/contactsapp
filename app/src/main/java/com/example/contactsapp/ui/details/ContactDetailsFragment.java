package com.example.contactsapp.ui.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.contactsapp.R;
import com.example.contactsapp.data.network.model.ContactModel;
import com.example.contactsapp.databinding.FragmentContactDetailsBinding;
import com.google.gson.Gson;

public class ContactDetailsFragment extends Fragment {
    private FragmentContactDetailsBinding binding;

    public static String CONTACT_DATA_TAG = "CONTACT_DATA_TAG";
    private ContactModel contactData = new ContactModel();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentContactDetailsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        parseContactData();
        setupView();
    }

    private void parseContactData() {
        if (getArguments() != null) {
            String jsonData = getArguments().getString(CONTACT_DATA_TAG, "");
            contactData = new Gson().fromJson(jsonData, ContactModel.class);
        }
    }

    private void setupView() {
        binding.firstName.setText(contactData.getFirst_name());
        binding.lastName.setText(contactData.getLast_name());
        binding.phoneNumber.setText(contactData.getPhone_number());

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .error(R.mipmap.ic_launcher_round);
        Glide.with(this).load(contactData.getAvatar()).apply(options).into(binding.avatar);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
