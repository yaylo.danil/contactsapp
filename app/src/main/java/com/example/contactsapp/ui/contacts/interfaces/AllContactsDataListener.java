package com.example.contactsapp.ui.contacts.interfaces;

import com.example.contactsapp.data.network.model.ContactModel;

import java.util.List;

public interface AllContactsDataListener {
    void onDataLoaded(List<ContactModel> list);

    void isContactChecked(Boolean isChecked, Integer index);

    void onError(Throwable throwable);
}
