package com.example.contactsapp.ui.contacts.all;

import android.annotation.SuppressLint;

import com.example.contactsapp.data.db.ContactsDao;
import com.example.contactsapp.data.network.ApiService;
import com.example.contactsapp.data.network.model.ContactModel;
import com.example.contactsapp.ui.contacts.interfaces.AllContactsDataListener;
import com.example.contactsapp.utils.ModelMapper;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AllContactsModel {
    private final ApiService apiService;
    private final ContactsDao contactsDao;
    private final AllContactsDataListener listener;

    private final ModelMapper mapper = new ModelMapper();

    private final int CONTACTS_DATA_COUNT = 15;

    public AllContactsModel(ApiService apiService, ContactsDao contactsDao, AllContactsDataListener listener) {
        this.apiService = apiService;
        this.contactsDao = contactsDao;
        this.listener = listener;
    }

    @SuppressLint("CheckResult")
    public void loadData() {
        apiService.getUsers(CONTACTS_DATA_COUNT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        listener::onDataLoaded,
                        listener::onError
                );
    }

    @SuppressLint("CheckResult")
    public void checkIsFavourite(ContactModel contact, Integer index) {
        contactsDao.isContactExist(contact.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        isChecked -> {
                            listener.isContactChecked(isChecked, index);
                        },
                        listener::onError
                );
    }

    @SuppressLint("CheckResult")
    public void insertContact(ContactModel contact) {
        contactsDao.insert(mapper.mapNetworkToDb(contact))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(Throwable::printStackTrace)
                .subscribe();
    }

    @SuppressLint("CheckResult")
    public void removeContact(ContactModel contact) {
        contactsDao.delete(mapper.mapNetworkToDb(contact))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(Throwable::printStackTrace)
                .subscribe();
    }
}
