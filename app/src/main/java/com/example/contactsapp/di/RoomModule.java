package com.example.contactsapp.di;

import android.content.Context;

import androidx.room.Room;

import com.example.contactsapp.data.db.AppDatabase;
import com.example.contactsapp.data.db.ContactsDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    @Singleton
    @Provides
    AppDatabase providesRoomDatabase(Context context) {
        return Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "contacts-db").build();
    }

    @Singleton
    @Provides
    ContactsDao providesProductDao(AppDatabase demoDatabase) {
        return demoDatabase.getContactsDao();
    }
}
