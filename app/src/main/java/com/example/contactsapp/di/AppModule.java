package com.example.contactsapp.di;

import android.content.Context;

import com.example.contactsapp.BaseApplication;
import com.example.contactsapp.data.db.ContactsDao;
import com.example.contactsapp.data.network.ApiService;
import com.example.contactsapp.ui.contacts.all.AllContactsPresenterImpl;
import com.example.contactsapp.ui.contacts.favourite.FavouriteContactsPresenterImpl;
import com.example.contactsapp.ui.contacts.interfaces.AllContactsContract;
import com.example.contactsapp.ui.contacts.interfaces.FavouriteContactsContract;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private BaseApplication application;

    public AppModule(BaseApplication app) {
        application = app;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return application;
    }


    @Provides
    public static AllContactsContract.Presenter provideAllContactsPresenter(ApiService api, ContactsDao contactsDao) {
        return new AllContactsPresenterImpl(api, contactsDao);
    }

    @Provides
    public static FavouriteContactsContract.Presenter provideFavouriteContactsPresenter(ContactsDao contactsDao) {
        return new FavouriteContactsPresenterImpl(contactsDao);
    }
}
