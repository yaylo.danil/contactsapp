package com.example.contactsapp.di;

import com.example.contactsapp.ui.contacts.all.AllContactsPageFragment;
import com.example.contactsapp.ui.contacts.favourite.FavouriteContactsPageFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, RoomModule.class})
public interface AppComponent {
    void inject(AllContactsPageFragment fragment);

    void inject(FavouriteContactsPageFragment fragment);
}