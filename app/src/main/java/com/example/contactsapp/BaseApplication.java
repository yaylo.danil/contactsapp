package com.example.contactsapp;

import android.app.Application;

import com.example.contactsapp.di.AppComponent;
import com.example.contactsapp.di.AppModule;
import com.example.contactsapp.di.DaggerAppComponent;
import com.example.contactsapp.di.NetworkModule;
import com.example.contactsapp.di.RoomModule;


public class BaseApplication extends Application {
    AppComponent appComponent = DaggerAppComponent.builder()
            .appModule(new AppModule(this))
            .roomModule(new RoomModule())
            .networkModule(new NetworkModule())
            .build();

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
