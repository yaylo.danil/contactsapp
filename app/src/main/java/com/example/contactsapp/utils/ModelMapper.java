package com.example.contactsapp.utils;

import com.example.contactsapp.data.db.model.Contact;
import com.example.contactsapp.data.network.model.ContactModel;

public class ModelMapper {
    public Contact mapNetworkToDb(ContactModel contactModel) {
        return new Contact(
                contactModel.getId(),
                contactModel.getFirst_name(),
                contactModel.getLast_name(),
                contactModel.getPhone_number(),
                contactModel.getAvatar()
        );
    }

    public ContactModel mapDbToNetwork(Contact contact, Boolean isFavourite) {
        ContactModel networkModel = new ContactModel();
        networkModel.setId(contact.id);
        networkModel.setFirst_name(contact.firstName);
        networkModel.setLast_name(contact.lastName);
        networkModel.setPhone_number(contact.phoneNumber);
        networkModel.setAvatar(contact.avatar);
        networkModel.setIsFavourite(isFavourite);
        return networkModel;
    }
}
